#define _GNU_SOURCE
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/select.h>
#include <unistd.h>
#include <fcntl.h>
#include <stdio.h>
#include <err.h>

int main(int argc, char *argv[])
{
	int fd, ret;

	if (argc != 2)
		errx(1, "usage: %s file_to_get write lease on", argv[0]);

	fd = open(argv[1], O_RDWR|O_CREAT, 0666);
	if (fd == -1)
		err(1, "open");
	ret = fcntl(fd, F_SETLEASE, F_WRLCK);
	if (ret == -1)
		err(1, "fcntl");
	ret = fork();
	if (ret < 0)
		err(1, "fork");
	if (ret > 0)
		printf("parent (%d) got lease, forked child (%d); "
				"both now waiting\n", getpid(), ret);
	select(0, NULL, NULL, NULL, NULL);
	exit(0);
}
