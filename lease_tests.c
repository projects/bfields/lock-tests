/* See COPYING for copyright and license */
#define _GNU_SOURCE
#include <errno.h>
#include <sys/types.h>
#include <sys/time.h>
#include <time.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>
#include <signal.h>
#include <fcntl.h>
#include <err.h>

int parentpipe[2];
int childpipe[2];
int signalpipe[2];
char *filename;
int signalled = 0;

void wait(int *pipe)
{
	ssize_t bytes;
	char byte[1];

	bytes = read(pipe[0], byte, 1);
	if (!(bytes == 1))
		err(1, "wait");
}

void wait_timeout(int *pipe, int seconds)
{
	ssize_t bytes;
	int ret;
	fd_set rfds;
	struct timeval tv = {.tv_sec = seconds, .tv_usec = 0};
	char byte[1];

	FD_ZERO(&rfds);
	FD_SET(pipe[0], &rfds);
	ret = select(1, &rfds, NULL, NULL, &tv);
	if (ret == -1 && errno != EINTR)
		err(1, "select");
	if (ret) {
		bytes = read(pipe[0], byte, 1);
		if (!(bytes == 1))
			err(1, "read");
	}
}

void wake(int *pipe)
{
	ssize_t bytes;

	bytes = write(pipe[1], "o", 1);
	if (bytes != 1)
		err(1, "wake");
}

void wait_for_child()
{
	wait(parentpipe);
}

void wake_parent()
{
	wake(parentpipe);
}

void wait_for_parent()
{
	wait(childpipe);
}

void wake_child()
{
	wake(childpipe);
}

void parent_sighand(int sig)
{
	signalled = 1;
	wake(signalpipe);
}

void simple_sighand(int sig)
{
	signalled = 1;
}

void setup_sighand(void (*sighand)(int))
{
	struct sigaction sa = {
		.sa_handler = sighand,
		.sa_flags = SA_RESTART,
	};
	int ret;

	ret = sigaction(SIGIO, &sa, NULL);
	if (ret)
		err(1, "sigaction");
}

void getlease_expect_type(int fd, int type)
{
	int ret;

	ret = fcntl(fd, F_GETLEASE);
	if (ret != type)
		errx(1, "F_GETLEASE expected %d, got %d\n", type, ret);
}

void setlease_expect_success(int fd, int type)
{
	int ret;

	ret = fcntl(fd, F_SETLEASE, type);
	if (ret)
		err(1, "fcntl(fd, F_SETLEASE, %d) failed", type);
}

void setlease_expect_conflict(int fd, int type)
{
	int ret;

	ret = fcntl(fd, F_SETLEASE, type);
	if (ret != -1 || errno != EAGAIN)
		errx(1, "F_SETLEASE expected ret = -1, errno = EAGAIN, "
			"got ret = %d, errno = %d\n", ret, errno);
}

void setsigown_expect_success(int fd, int signum, int owner)
{
	int ret;

	ret = fcntl(fd, F_SETSIG, signum);
	if (ret)
		err(1, "fcntl(fd, F_SETSIG, %d) failed", signum);
	ret = fcntl(fd, F_SETOWN, owner);
	if (ret)
		err(1, "fcntl(fd, F_SETOWN, %d) failed", owner);
}

void getsigown_expect_sigown(int fd, int signum, pid_t owner)
{
	int ret;

	ret = fcntl(fd, F_GETSIG);
	if (ret != signum)
		errx(1, "F_GETSIG expected %d, got %d", signum, ret);
	ret = fcntl(fd, F_GETOWN);
	if (ret != owner)
		errx(1, "F_GETOWN expected %d, got %d", owner, ret);
}

void open_expect_conflict(int type)
{
	int fd;

	fd = open(filename, O_NONBLOCK|type, 0666);
	/* Is EINTR also possible? */
	if (fd != -1 || errno != EWOULDBLOCK)
		errx(1, "open expected ret = -1, errno = EWOULDBLOCK, "
			"got ret = %d, errno = %d\n", fd, errno);
	/* XXX: Are we sure we have to have gotten the signal by now?: */
	if (!signalled)
		errx(1, "expected to be signalled\n");
	signalled = 0;
}

int get_lease_wait_for_break(long lease_type)
{
	int fd, ret, broken;

	fd = open(filename, O_RDONLY|O_CREAT, 0666);
	if (fd == -1)
		err(1, "open");
	ret = fcntl(fd, F_SETLEASE, lease_type);
	if (ret)
		err(1, "fcntl");
	wake_child();
	wait_timeout(signalpipe, 1);
	broken = signalled;
	close(fd);
	wait_for_child(); /* give child a chance to close the file */
	signalled = 0;
	return broken;
}

void wait_for_break()
{
	int fd;
	char buf[10];
	ssize_t bytes;
	int leasetime;
	time_t begin, end;

	begin = time(NULL);
	fd = open(filename, O_RDWR);
	if (fd == -1)
		err(1, "open");
	end = time(NULL);
	close(fd);
	fd = open("/proc/sys/fs/lease-break-time", O_RDONLY);
	if (fd == -1)
		err(1, "open");
	bytes = read(fd, buf, sizeof(buf));
	if (bytes == -1)
		err(1, "read");
	leasetime = atoi(buf);
	if (end - begin > leasetime + 1)
		errx(1, "waited %ld > %d for lease timeout", end-begin, leasetime);
	if (end - begin < leasetime)
		errx(1, "waited %ld < %d for lease timeout", end-begin, leasetime);
}

void do_parent(pid_t child)
{
	int broken, ret;

	ret = pipe(signalpipe);
	if (ret)
		err(1, "pipe");

	setup_sighand(parent_sighand);
	broken = get_lease_wait_for_break(F_WRLCK);
	if (!broken)
		errx(1, "exclusive lease not broken by read open");
	broken = get_lease_wait_for_break(F_WRLCK);
	if (!broken)
		errx(1, "exclusive lease not broken by write open");
	broken = get_lease_wait_for_break(F_RDLCK);
	if (!broken)
		errx(1, "shared lease not broken by write open");
	broken = get_lease_wait_for_break(F_RDLCK);
	if (broken)
		errx(1, "shared lease broken by read open");
	wake_child();
	wait_for_child(); /* child has lease on return */
	wait_for_break();
	wake_child(); /* child sleeps to hold file open with lease */

	/* todo: */
	/* verify that exclusive leases conflict */
	/* verify that shared leases do not conflict */
	exit(0);
}

void do_child_open(int flags)
{
	int fd, ret;

	wait_for_parent();
	fd = open(filename, flags, 0);
	if (fd == -1)
		err(1, "open");
	ret = close(fd);
	if (ret == -1)
		err(1, "close");
	wake_parent();
}

void do_unlink(void)
{
	int ret;

	wait_for_parent();
	ret = unlink(filename);
	if (ret == -1)
		err(1, "unlink");
	wake_parent();
}

void do_child(void)
{
	int fd;

	do_child_open(O_RDONLY);
	do_child_open(O_WRONLY);
	do_child_open(O_WRONLY);
	do_child_open(O_RDONLY);
	fd = open(filename, O_RDONLY|O_CREAT, 0666);
	if (fd == -1)
		err(1, "open");
	wait_for_parent();
	setlease_expect_success(fd, F_WRLCK);
	wake_parent();
	wait_for_parent();
	exit(0);
}

void singleprocess_tests()
{
	int fd, fd2, pid;

	setup_sighand(simple_sighand);

	fd = open(filename, O_RDONLY|O_CREAT, 0666);
	if (fd == -1)
		err(1, "initial open failed");

	/* Simple getlease tests: */
	getlease_expect_type(fd, F_UNLCK);
	setlease_expect_success(fd, F_RDLCK);
	getlease_expect_type(fd, F_RDLCK);
	setlease_expect_success(fd, F_UNLCK);
	getlease_expect_type(fd, F_UNLCK);
	setlease_expect_success(fd, F_WRLCK);
	getlease_expect_type(fd, F_WRLCK);
	setlease_expect_success(fd, F_RDLCK);
	
	/* Check for self-conflicts with opens: */
	open_expect_conflict(O_RDWR);

	/* Should succeed as it doesn't conflict with read lease */
	fd2 = open(filename, O_NONBLOCK|O_RDONLY, 0666);
	if (fd2 == -1)
		err(1, "open");
	close(fd2);
	/* We're allowed to change our own lease, though that doesn't
	 * get us out if it being forcibly revoked if we don't remove it
	 * soon....: */
	setlease_expect_success(fd, F_WRLCK);
	close(fd);
	/* Verify that lease was destroyed on last close of fd: */
	fd = open(filename, O_RDWR|O_NONBLOCK, 0666);
	if (fd == -1)
		err(1, "open");
	/* Verify that read lease conflicts even with own open: */
	setlease_expect_conflict(fd, F_RDLCK);
	close(fd);

	/* Check that calling F_SETLEASE F_UNLCK will zero out
	 * the settings of F_SETOWN and F_SETSIG:
	 */
	pid = getpid();
	fd = open(filename, O_RDONLY, 0666);
	if (fd == -1)
		err(1, "open");
	setsigown_expect_success(fd, SIGUSR1, pid);
	setlease_expect_success(fd, F_WRLCK);
	getsigown_expect_sigown(fd, SIGUSR1, pid);
	setlease_expect_success(fd, F_UNLCK);
	getsigown_expect_sigown(fd, 0, 0);
	close(fd);

	/* Check that calling F_SETLEASE twice won't change
	 * the settings of F_SETOWN and F_SETSIG:
	 * https://bugzilla.kernel.org/show_bug.cgi?id=43336
	 */
	fd = open(filename, O_RDONLY, 0666);
	if (fd == -1)
		err(1, "open");
	setsigown_expect_success(fd, SIGUSR1, pid);
	setlease_expect_success(fd, F_WRLCK);
	getsigown_expect_sigown(fd, SIGUSR1, pid);
	setlease_expect_success(fd, F_WRLCK);
	getsigown_expect_sigown(fd, SIGUSR1, pid);
	close(fd);
}

void check_leases_supported(void)
{
	int fd, ret;

	fd = open(filename, O_RDONLY|O_CREAT, 0666);
	if (fd == -1)
		err(1, "initial open failed");
	ret = fcntl(fd, F_SETLEASE, F_WRLCK);
	/* Hey, it's perfectly legal not to support leases at all: */
	if (ret && errno == EINVAL) {
		printf("skipping lease tests; leases not supported\n");
		exit(0);
	}
	close(fd);
}

int main(int argc, char *argv[])
{
	int ret;
	pid_t p;

	if (argc != 2)
		errx(1, "usage: %s path", argv[0]);
	filename = argv[1];

	check_leases_supported();

	singleprocess_tests();

	/* two-process tests: */
	ret = pipe(parentpipe);
	if (ret)
		err(1, "pipe");
	ret = pipe(childpipe);
	if (ret)
		err(1, "pipe");
	p = fork();
	if (p == -1)
		err(1, "fork");
	if (p)
		do_parent(p);
	do_child();

	return 0;
}
