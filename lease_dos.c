#include <unistd.h>
#include <stdlib.h>
#include <linux/fcntl.h>

int main(int ac, char **av)

{
	char *fname = av[1];
	int fd = open(fname, O_RDONLY);
	int r, i;

	if (fd == -1) {
		perror("open");
		exit(1);
	}
	for (i=0; i<10000; i++) {
		r = fcntl(fd, F_SETLEASE, F_RDLCK);
		if (r == -1) {
			perror("F_SETLEASE, F_RDLCK");
			exit(1);
		}
		r = fcntl(fd, F_SETLEASE, F_UNLCK);
		if (r == -1) {
			perror("F_SETLEASE, F_UNLCK");
			exit(1);
		}
	}
}
