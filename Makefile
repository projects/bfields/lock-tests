CFLAGS=-lpthread -Wall -g

testfile = TESTFILE

all: lease_tests flock_tests plock_tests
clean:
	rm -f lease_tests flock_tests plock_tests

test: all
	rm -f $(testfile)
	./flock_tests $(testfile)
	rm -f $(testfile)
	./plock_tests $(testfile)
