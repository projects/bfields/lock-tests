/* See COPYING for copyright and license */
#define _GNU_SOURCE
#include <errno.h>
#include <sys/types.h>
#include <sys/file.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>
#include <signal.h>
#include <fcntl.h>
#include <err.h>

char *filename;

int open_expect_success(int type)
{
	int ret;

	ret = open(filename, type, 0666);
	if (ret == -1)
		err(1, "open");
	return ret;
}

void flock_expect_success(int fd, int type)
{
	int ret;

	ret = flock(fd, type);
	if (ret)
		err(1, "flock");
}

void flock_expect_conflict(int fd, int type)
{
	int ret;

	ret = flock(fd, type|LOCK_NB);
	if (ret == 0)
		errx(1, "unexpected flock success");
	if (errno != EWOULDBLOCK)
		err(1, "unexpected flock error");
}

void singleprocess_tests()
{
	int fd, fd2;

	fd = open_expect_success(O_RDWR|O_CREAT);
	flock_expect_success(fd, LOCK_SH);
	flock_expect_success(fd, LOCK_EX);
	flock_expect_success(fd, LOCK_UN);
	flock_expect_success(fd, LOCK_EX);

	fd2 = open_expect_success(O_RDWR);
	flock_expect_conflict(fd2, LOCK_SH);
	flock_expect_conflict(fd2, LOCK_EX);

	flock_expect_success(fd, LOCK_SH);
	flock_expect_success(fd2, LOCK_SH);

	close(fd);

	flock_expect_success(fd2, LOCK_EX);
	close(fd2);
}

int main(int argc, char *argv[])
{
	if (argc != 2)
		errx(1, "usage: %s path", argv[0]);
	filename = argv[1];

	singleprocess_tests();

	return 0;
}
