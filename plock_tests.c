#include <sys/types.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <sys/wait.h>
#include <errno.h>
#include <signal.h>
#include <pthread.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <stdio.h>
#include <err.h>

int fd;

int __lock_one_byte(int offset)
{
	struct flock fl = {
		.l_type = F_WRLCK,
		.l_whence = SEEK_SET,
		.l_start = offset,
		.l_len = 1,
	};
	int ret;

	ret = fcntl(fd, F_SETLKW, &fl);
	if (ret)
		return errno;
	return 0;
}

void lock_one_byte(int offset)
{
	int ret;

	ret = __lock_one_byte(offset);
	if (ret)
		err(1, "fcntl");
}

void lock_one_byte_expect_err(int offset, int expected)
{
	int ret;

	ret = __lock_one_byte(offset);
	if (ret == 0)
		errx(1, "fcntl: unexpected success\n");
	if (ret != expected)
		err(1, "fcntl");
}

struct cmd {
	void (*f)(int);
	int arg;
};

struct slave {
	pid_t pid;
	int cmdpipe[2];
	int respipe[2];
};

void slave_driver(struct slave *slave)
{
	struct cmd c;

	while (1) {
		read(slave->cmdpipe[0], &c, sizeof(c));
		c.f(c.arg);
		write(slave->respipe[1], "z", 1);
	}
}

void pipex(int *pfd)
{
	int ret;

	ret = pipe(pfd);
	if (ret == -1)
		err(1, "pipe");
}

struct slave *alloc_slave(void)
{
	struct slave *slave;

	slave = malloc(sizeof(struct slave));
	if (slave == NULL)
		err(1, "malloc");
	pipex(slave->cmdpipe);
	pipex(slave->respipe);
	return slave;
}

struct slave *slave_proc(void)
{
	struct slave *slave;
	int ret;

	slave = alloc_slave();
	ret = fork();
	if (ret == -1)
		err(1, "fork");
	if (ret == 0)
		slave_driver(slave);
	slave->pid = ret;
	return slave;
}

void *do_slave_thread(void *slave)
{
	slave_driver(slave);
	return NULL;
}

struct slave *slave_proc_plus_thread(struct slave **tslave)
{
	struct slave *slave;
	pthread_t th;
	int ret;

	slave = alloc_slave();
	*tslave = alloc_slave();
	ret = fork();
	if (ret == -1)
		err(1, "fork");
	if (ret == 0) {
		ret = pthread_create(&th, NULL, do_slave_thread, *tslave);
		if (ret) /* does pthread_create set errno?? */
			err(ret, "pthread_create %d", ret);
		slave_driver(slave);
	}
	(*tslave)->pid = 0;
	slave->pid = ret;
	return slave;
}

void runon(struct slave *slave, void (*f)(int), int arg)
{
	struct cmd c = {
		.f = f,
		.arg = arg,
	};

	write(slave->cmdpipe[1], &c, sizeof(c));
}

void runon_wait(struct slave *slave, void (*f)(int), int arg)
{
	char r;

	runon(slave, f, arg);

	/* wait till done: */
	read(slave->respipe[0], &r, 1);
}

void runon_delay(struct slave *slave, void (*f)(int), int arg)
{
	runon(slave, f, arg);

	/*
	 * we expect this operation to block; delay to give it time to
	 * do so:
	 */
	sleep(1);
}

void thread_suicide(int arg)
{
	pthread_exit(NULL);
}

void handler(int sig)
{
	return;
}

void set_timer(void)
{
	struct itimerval it = {
		.it_interval = {},
		.it_value = {
			.tv_sec = 1,
			.tv_usec = 0,
		},
	};
	struct sigaction act = {
		.sa_handler = handler,
	};
	int ret;

	ret = sigaction(SIGALRM, &act, NULL);
	if (ret)
		err(1, "sigaction");
	ret = setitimer(ITIMER_REAL, &it, NULL);
	if (ret)
		err(1, "setitimer");
}

void kill_slave(struct slave *slave)
{
	if (slave->pid) {
		kill(slave->pid, SIGKILL);
		waitpid(slave->pid, NULL, 0);
	}
	free(slave);
}

void test1(void)
{
	struct slave *p1, *p2, *p2t;
	int ret;

	p1 = slave_proc();
	p2 = slave_proc_plus_thread(&p2t);

	/* These succeed: */
	lock_one_byte(1);
	runon_wait(p1, lock_one_byte, 2);
	runon_wait(p2, lock_one_byte, 3);

	/* These block: */
	runon_delay(p2, lock_one_byte, 2);
	runon_delay(p2t, lock_one_byte, 1);
	runon_delay(p1, lock_one_byte, 3);

	/*
	 * That last lock does not trigger deadlock detection: p1's
	 * lock conflicts with that held by p2.  But a search for blocks
	 * held by p2 will first turn up p2t's request on 1, which blocks
	 * on the current process, which holds no locks.  That search
	 * will not see p2's request for 2 because blocks are inserted
	 * onto the head of the list of blocking locks, so p2t's block
	 * (more recently inserted) will be found first.
	 */

	/* Kill p2t, so its block will no longer hide p2's. */
	runon(p2t, thread_suicide, 0);

	/*
	 * On success, the following lock will "just" deadlock instead
	 * of freezing the kernel; set a timer to wake us up so we can
	 * recognize the succesful case.
	 */
	set_timer();
	/*
	 * There is now a simple loop: p2 is blocking on a request for
	 * byte 2, which is locked by p1, which is blocking on a request
	 * for byte 3, which is locked by p2.
	 */
	/*
	 * The following request will turn up p1's lock as conflicting.
	 * A search for locks p1 is blocking on will find p2's lock.  A
	 * search for locks p2 is blocking on will find p1's lock.  Etc.
	 * An infinite loop, with the BKL held!
	 */
	/* Actually, either EDEADLK or EINTR are acceptable: */
	__lock_one_byte(2);
	ret = __lock_one_byte(2);
	if (ret != EDEADLK && ret != EINTR)
		errx(1, "fcntl");

	kill_slave(p2t);
	kill_slave(p2);
	kill_slave(p1);
}

void test2(void)
{
	struct slave *p1;

	p1 = slave_proc();

	/* basic test for deadlock detection: */

	lock_one_byte(5);
	runon_wait(p1, lock_one_byte, 4);

	runon_delay(p1, lock_one_byte, 5);
	lock_one_byte_expect_err(4, EDEADLK);

	kill_slave(p1);
}

int main(int argc, char **argv)
{
	if (argc != 2)
		errx(1, "usage: %s path", argv[0]);
	fd = open(argv[1], O_CREAT|O_RDWR, 0666);
	if (fd == -1)
		err(1, "open");

	test1();
	test2();

	return 0;
}
