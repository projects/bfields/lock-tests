#include <sys/types.h>
#include <sys/stat.h>
#include <limits.h>
#include <errno.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <stdio.h>
#include <err.h>

int fd;

void lock_range(int fd, off_t seek_to, off_t start, off_t len)
{
	struct flock fl = {
		.l_type = F_WRLCK,
		.l_whence = SEEK_CUR,
		.l_start = start,
		.l_len = len,
	};
	int w = sizeof(long) * 2;
	off_t ret;

	ret = lseek(fd, seek_to, SEEK_SET);
	printf("seek(0x%0*lx) = %3d, ", w, seek_to,
			ret == -1 ? errno : 0);
	if (ret == -1) {
		printf("\n");
		return;
	}
	ret = fcntl(fd, F_SETLKW, &fl);
	printf("lock(0x%0*lx, 0x%0*lx) = %3d\n", w, start, w, len,
			ret == -1 ? errno : 0);
}

int main(int argc, char **argv)
{
	if (argc != 2)
		errx(1, "usage: %s path", argv[0]);
	fd = open(argv[1], O_CREAT|O_RDWR, 0666);
	if (fd == -1)
		err(1, "open");

	lock_range(fd, 0, 0, 0);
	lock_range(fd, LONG_MAX, 0, 0);
	lock_range(fd, 0, LONG_MAX, 0);
	lock_range(fd, 0, 0, LONG_MAX);
	lock_range(fd, LONG_MAX, LONG_MAX, 0);
	lock_range(fd, LONG_MAX, 0, LONG_MAX);
	lock_range(fd, 0, LONG_MAX, LONG_MAX);
	lock_range(fd, LONG_MAX, LONG_MAX, LONG_MAX);
	lock_range(fd, LONG_MAX/2, LONG_MAX/2, LONG_MAX/2);
	lock_range(fd, 0, -1, 0);

	return 0;
}
